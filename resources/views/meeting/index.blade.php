<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body class="antialiased">
  <div class="table-responsive">
    <table id="myTable" class="table table-hover table-bordered">
      <thead>
        <th width="5%">ID</th>
        <th>Name</th>
      </thead>
      <tbody id="majorBody"></tbody>
    </table>
  </div>

  <script>
    $(document).ready(function() {
      $.ajax({
        url: "http://127.0.0.1:8000/api/v1/meeting",
        type: "GET",
        dataType: 'json',
        success: function(result) {
          for (var d in result) {
            var data = result[d];
            // console.log(data);
            for (var c in data) {
              var data_meeting = data[c];
              $('#majorBody').append($('<tr>')
                .append($('<td>', {
                  text: data_meeting['id']
                }))
                .append($('<td>', {
                  text: data_meeting['title']
                }))
                .append($('<td>', {
                  text: data_meeting['description']
                }))
              )
            }
          }
        }
      });
    });
  </script>
</body>

</html>